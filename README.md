# Stack Calculator Project

Basic Calculator App with stack behaviour (inspired in HP48G).
Programmed in Python. GUI PyQt5.

![interface](.dependencies/images/stack_calc_image.png)
import os
import sys
import json
from PyQt5.QtWidgets import (QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, QGridLayout,
                             QFrame, QLabel, QListWidget, QPushButton, QLineEdit, QStatusBar)
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import Qt, QRect, QTranslator, QFile, QIODevice, QTextStream
from collections import deque
import multiprocessing

from stack_calc_class import Stack_Calculator, Stack_Label, Stack_QPushButton
import project_constants as pc



class MainWindow(QMainWindow):
    """
    This Class defines all the GUI objects and functionality
    """

    def __init__(self):
        super().__init__()
        self.initUI()
        self.init_functionality()
        self.load_data()


    def initUI(self):
        """
        Defines all the GUI objects in the application
        """

        # central widget
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)
        self.central_widget.setStyleSheet(f"background-color:{pc.APP_BG_COLOR};")

        # vertical layout ruling everything
        main_layout = QVBoxLayout()
        self.central_widget.setLayout(main_layout)

        # screen
        self.screen_widget = QFrame()
        screen_layout = QVBoxLayout()
        self.screen_widget.setLayout(screen_layout)
        self.screen_widget.setStyleSheet(f"background-color:{pc.SCREEN_BG_COLOR};")

        # up to 6 visible rows in the stack + line edit
        self.l6 = Stack_Label()
        screen_layout.addWidget(self.l6)
        self.l5 = Stack_Label()
        screen_layout.addWidget(self.l5)
        self.l4 = Stack_Label()
        screen_layout.addWidget(self.l4)
        self.l3 = Stack_Label()
        screen_layout.addWidget(self.l3)
        self.l2 = Stack_Label()
        screen_layout.addWidget(self.l2)
        self.l1 = Stack_Label()
        screen_layout.addWidget(self.l1)
        self.le = Stack_Label()
        screen_layout.addWidget(self.le)
        self.le.setStyleSheet(f'color: {pc.LINE_EDIT_TEXT};')

        main_layout.addWidget(self.screen_widget)

        # Vars block. Grid Layout 2 x 4
        self.vars_widget = QFrame()
        vars_layout = QGridLayout()
        self.vars_widget.setLayout(vars_layout)

        # labels to inform the value assigned to the vars
        self.lbl_a = QLabel()
        vars_layout.addWidget(self.lbl_a, 0, 0)
        self.lbl_b = QLabel()
        vars_layout.addWidget(self.lbl_b, 0, 1)
        self.lbl_c = QLabel()
        vars_layout.addWidget(self.lbl_c, 0, 2)
        self.lbl_d = QLabel()
        vars_layout.addWidget(self.lbl_d, 0, 3)

        for label in [self.lbl_a, self.lbl_b, self.lbl_c, self.lbl_d]:
            label.setAlignment(Qt.AlignCenter)

        # buttons for the vars
        self.btn_A = Stack_QPushButton('normal', 'A')
        vars_layout.addWidget(self.btn_A, 1, 0)
        self.btn_B = Stack_QPushButton('normal', 'B')
        vars_layout.addWidget(self.btn_B, 1, 1)
        self.btn_C = Stack_QPushButton('normal', 'C')
        vars_layout.addWidget(self.btn_C, 1, 2)
        self.btn_D = Stack_QPushButton('normal', 'D')
        vars_layout.addWidget(self.btn_D, 1, 3)

        main_layout.addWidget(self.vars_widget)

        # not so common operations block
        self.notcommon_buttons_widget = QFrame()
        notcommon_buttons_layout = QHBoxLayout()
        self.notcommon_buttons_widget.setLayout(notcommon_buttons_layout)

        self.btn_INV = Stack_QPushButton('normal', '1/x')
        notcommon_buttons_layout.addWidget(self.btn_INV)
        self.btn_SQRT = Stack_QPushButton('normal', 'sqrt')
        notcommon_buttons_layout.addWidget(self.btn_SQRT)
        self.btn_POW = Stack_QPushButton('normal', 'x^y')
        notcommon_buttons_layout.addWidget(self.btn_POW)
        self.btn_LN = Stack_QPushButton('normal', 'ln')
        notcommon_buttons_layout.addWidget(self.btn_LN)
        self.btn_LOG = Stack_QPushButton('normal', 'log')
        notcommon_buttons_layout.addWidget(self.btn_LOG)

        main_layout.addWidget(self.notcommon_buttons_widget)

        # special buttons ENTER - SUPR - SWAP block
        self.special_buttons_widget = QFrame()
        special_buttons_layout = QHBoxLayout()
        self.special_buttons_widget.setLayout(special_buttons_layout)

        self.btn_ENTER = Stack_QPushButton('normal', 'ENTER')
        special_buttons_layout.addWidget(self.btn_ENTER)
        self.btn_SUPR = Stack_QPushButton('normal', 'SUPR')
        special_buttons_layout.addWidget(self.btn_SUPR)
        self.btn_SWAP = Stack_QPushButton('normal', 'SWAP')
        special_buttons_layout.addWidget(self.btn_SWAP)

        main_layout.addWidget(self.special_buttons_widget)

        # regular buttons block
        self.buttons_widget = QFrame()
        buttons_layout = QGridLayout()
        self.buttons_widget.setLayout(buttons_layout)

        self.btn_VAR = Stack_QPushButton('normal', 'VAR')
        self.btn_VAR.setCheckable(True)
        buttons_layout.addWidget(self.btn_VAR, 0, 0)
        self.btn_DOWN = Stack_QPushButton('disabled', '')
        buttons_layout.addWidget(self.btn_DOWN, 1, 0)
        self.btn_DEL = Stack_QPushButton('normal', 'DEL')
        buttons_layout.addWidget(self.btn_DEL, 2, 0)
        self.btn_CANCEL = Stack_QPushButton('normal', 'CANCEL')
        buttons_layout.addWidget(self.btn_CANCEL, 3, 0)

        self.btn_7 = Stack_QPushButton('special', '7')
        buttons_layout.addWidget(self.btn_7, 0, 1)
        self.btn_4 = Stack_QPushButton('special', '4')
        buttons_layout.addWidget(self.btn_4, 1, 1)
        self.btn_1 = Stack_QPushButton('special', '1')
        buttons_layout.addWidget(self.btn_1, 2, 1)
        self.btn_0 = Stack_QPushButton('special', '0')
        buttons_layout.addWidget(self.btn_0, 3, 1)

        self.btn_8 = Stack_QPushButton('special', '8')
        buttons_layout.addWidget(self.btn_8, 0, 2)
        self.btn_5 = Stack_QPushButton('special', '5')
        buttons_layout.addWidget(self.btn_5, 1, 2)
        self.btn_2 = Stack_QPushButton('special', '2')
        buttons_layout.addWidget(self.btn_2, 2, 2)
        self.btn_DOT = Stack_QPushButton('special', '.')
        buttons_layout.addWidget(self.btn_DOT, 3, 2)

        self.btn_9 = Stack_QPushButton('special', '9')
        buttons_layout.addWidget(self.btn_9, 0, 3)
        self.btn_6 = Stack_QPushButton('special', '6')
        buttons_layout.addWidget(self.btn_6, 1, 3)
        self.btn_3 = Stack_QPushButton('special', '3')
        buttons_layout.addWidget(self.btn_3, 2, 3)
        self.btn_PLUS_MINUS = Stack_QPushButton('special', '+/-')
        buttons_layout.addWidget(self.btn_PLUS_MINUS, 3, 3)

        self.btn_DIVISION = Stack_QPushButton('normal', '/')
        buttons_layout.addWidget(self.btn_DIVISION, 0, 4)
        self.btn_TIMES = Stack_QPushButton('normal', '*')
        buttons_layout.addWidget(self.btn_TIMES, 1, 4)
        self.btn_MINUS = Stack_QPushButton('normal', '-')
        buttons_layout.addWidget(self.btn_MINUS, 2, 4)
        self.btn_PLUS = Stack_QPushButton('normal', '+')
        buttons_layout.addWidget(self.btn_PLUS, 3, 4)

        main_layout.addWidget(self.buttons_widget)

        # footer
        self.footer_widget = QFrame()
        footer_layout = QHBoxLayout()
        self.footer_widget.setLayout(footer_layout)

        self.statusbar = QStatusBar()
        footer_layout.addWidget(self.statusbar)
        self.statusbar.showMessage('Ready', pc.TIMEOUT)

        self.signature = QLabel(pc.SIGNATURE)
        self.signature.setAlignment(Qt.AlignLeft)
        self.signature.setStyleSheet('color: dimgray;')
        footer_layout.addWidget(self.signature)

        main_layout.addWidget(self.footer_widget)

        # main window properties
        self.move(pc.OFFSET, pc.OFFSET)
        self.setFixedSize(pc.GLOBAL_WIDTH, pc.GLOBAL_HEIGHT)
        self.setWindowTitle(pc.APP_NAME)
        self.setWindowIcon(QIcon(pc.APP_ICON))
        self.show()


    def init_functionality(self):
        """
        Defines the Relationship between GUI objects, 
        the event that triggers the functionality
        and the functionality itself
        Ex: button -> clicked -> do_something
        """

        # instantiate backend
        self.stack = Stack_Calculator()

        # variables block
        self.btn_A.clicked.connect(lambda: self.variables_slot(self.lbl_a, 'A'))
        self.btn_B.clicked.connect(lambda: self.variables_slot(self.lbl_b, 'B'))
        self.btn_C.clicked.connect(lambda: self.variables_slot(self.lbl_c, 'C'))
        self.btn_D.clicked.connect(lambda: self.variables_slot(self.lbl_d, 'D'))

        # input numbers
        input_buttons = [self.btn_1, self.btn_2, self.btn_3, self.btn_4,
                         self.btn_5, self.btn_6, self.btn_7, self.btn_8,
                         self.btn_9, self.btn_0, self.btn_DOT]

        for btn in input_buttons:
            btn.clicked.connect(self.write_from_button)

        self.btn_CANCEL.clicked.connect(self.le.clear)
        self.btn_ENTER.clicked.connect(self.enter)
        self.btn_SUPR.clicked.connect(self.supress)
        self.btn_DEL.clicked.connect(self.delete)
        self.btn_PLUS_MINUS.clicked.connect(self.plus_minus)
        self.btn_SWAP.clicked.connect(self.swap)

        # binary calculations
        binary_operator_buttons = [self.btn_PLUS, self.btn_MINUS,
                                   self.btn_TIMES, self.btn_DIVISION,
                                   self.btn_POW]

        for btn in binary_operator_buttons:
            btn.clicked.connect(self.binary_operation)


        # unary calculations
        unary_operator_buttons = [self.btn_INV, self.btn_SQRT,
                                  self.btn_LN, self.btn_LOG]

        for btn in unary_operator_buttons:
            btn.clicked.connect(self.unary_operation)


    def keyPressEvent(self, e):
        """
        Reimplementation of method keyPressEvent 
        to allow the user to type numbers and basic operations
        from the keyboard (additionally to the mouse-click buttons)
        """
        if e.key() == Qt.Key_1:
            self.write_from_key('1')
        if e.key() == Qt.Key_2:
            self.write_from_key('2')
        if e.key() == Qt.Key_3:
            self.write_from_key('3')
        if e.key() == Qt.Key_4:
            self.write_from_key('4')
        if e.key() == Qt.Key_5:
            self.write_from_key('5')
        if e.key() == Qt.Key_6:
            self.write_from_key('6')
        if e.key() == Qt.Key_7:
            self.write_from_key('7')
        if e.key() == Qt.Key_8:
            self.write_from_key('8')
        if e.key() == Qt.Key_9:
            self.write_from_key('9')
        if e.key() == Qt.Key_0:
            self.write_from_key('0')
        if e.key() == Qt.Key_Period:
            self.write_from_key('.')

        if e.key() == Qt.Key_Enter:
            self.enter()
        if e.key() == Qt.Key_Delete:
            self.supress()
        if e.key() == Qt.Key_Backspace:
            self.delete()

        if e.key() == Qt.Key_Plus:
            self.binary_operation('+')
        if e.key() == Qt.Key_Minus:
            self.binary_operation('-')
        if e.key() == Qt.Key_Asterisk:
            self.binary_operation('*')
        if e.key() == Qt.Key_Slash:
            self.binary_operation('/')

        if e.key() == Qt.Key_Escape:
            self.le.clear()


    def refresh_screen(self):
        """
        Synchromize visible data on screen with data from the backend
        """
        screen_values = self.stack.items + deque([''] * 6)
        screen_values = [str(val) for val in screen_values] # strings to pass to labels

        screen_labels = [self.l1, self.l2, self.l3, self.l4, self.l5, self.l6]
        for i in range(6):
            screen_labels[i].setText(screen_values[i])


    def write_from_button(self):
        """
        Type with mouse-clicks
        """
        edition = self.le.text()
        number_to_add = self.sender().text()
        self.le.setText(edition + number_to_add)


    def write_from_key(self, char):
        """
        Type from keyboard
        """
        edition = self.le.text()
        self.le.setText(edition + char)


    def enter(self):
        """
        Implementation of the functionality when pressing ENTER button
        Sends the expression from LineEdit to the Stack
        If LineEdit is empty duplicate the last entry of the Stack
        """
        edition = self.le.text()
        if len(edition) > 0:
            valid_edition = self.stack.add(edition)
            self.le.clear()
            if not valid_edition:
                self.statusbar.showMessage('Malformed expression', pc.TIMEOUT)
        elif len(self.stack) > 0:
            self.stack.add(self.stack.items[0])
        self.refresh_screen()


    def supress(self):
        """
        Implementation of the functionality when pressing SUPR button
        Remove last entry of the Stack
        """
        self.stack.remove()
        self.refresh_screen()


    def delete(self):
        """
        Implementation of the functionality when pressing DEL button
        Delete last number in LineEdit
        """
        edition = self.le.text()
        if len(edition) > 0:
            self.le.setText(edition[:-1])
        self.refresh_screen()


    def plus_minus(self):
        """
        Implementation of the functionality when pressing +/- button
        Reverse the sign of the LineEdit
        If LineEdit is empty reverse the sign of the last entry of the Stack
        """
        edition = self.le.text()
        if len(edition) > 0:
            if edition[0] == '-':
                new_edition = edition[1:]
            else:
                new_edition = '-' + edition
            self.le.setText(new_edition)
        elif len(self.stack) > 0:
            self.stack.operate('+/-')
        self.refresh_screen()


    def swap(self):
        """
        Implementation of the functionality when pressing SWAP button
        Swap the position between the last two entries in the Stack
        """
        if len(self.stack) > 1:
            self.stack.swap()
        self.refresh_screen()


    def binary_operation(self, op=None):
        """
        Implementation of the functionality of binary operations,
        Operation takes the LineEdit value and the last entry in the Stack.
        If LineEdit is empty the operation takes the two last entries in the Stack
        """
        if not op:   # si no se explicita operacion la tomo del sender
            op = self.sender().text()
        edition = self.le.text()
        if len(edition) > 0 and len(self.stack) > 0:
            valid_edition = self.stack.add(edition)
            self.le.clear()
            if valid_edition:
                message = self.stack.operate(op)
            else:
                message = 'Malformed expression'

        elif len(edition) == 0 and len(self.stack) > 1:
            message = self.stack.operate(op)
        else:
            message = None
        self.refresh_screen()
        if message:
            self.statusbar.showMessage(message, pc.TIMEOUT)


    def unary_operation(self, op=None):
        """
        Implementation of the functionality of unary operations,
        Operation takes the LineEdit value.
        If LineEdit is empty the operation takes the last entry in the Stack
        """
        if not op:   # si no se explicita operacion la tomo del sender
            op = self.sender().text()
        edition = self.le.text()
        if len(edition) > 0:
            valid_edition = self.stack.add(edition)
            self.le.clear()
            if valid_edition:
                message = self.stack.operate(op)
            else:
                message = 'Malformed expression'

        elif len(edition) == 0 and len(self.stack) > 0:
            message = self.stack.operate(op)
        else:
            message = None
        self.refresh_screen()
        if message:
            self.statusbar.showMessage(message, pc.TIMEOUT)


    def short_label_num(self, num):
        """
        Short representation of a number used to show the value stored in a variable
        """
        max_len = 7
        len_num = len(str(num))
        if num == 0:
            return ''
        elif len_num <= 7:
            return str(num)
        elif '.' in str(num):
            integer_part, decimals_part = str(num).split('.')
            rounded_decimals = max_len - len(integer_part) - 1
            if rounded_decimals > 0:
                return str(round(num, rounded_decimals))
            else:
                return str(num)[:4] + '...'
        else:
            return str(num)[:4] + '...'


    def variables_slot(self, lbl, user_var):
        """
        Implementation of the functionality when pressing VAR button
        When VAR is chequed and a variable button (A,B,C,D) is clicked
        the value in LineEdit is stored in the variable. If LineEdit
        is empty, the last entry in the Stack is taken.
        """

        edition = self.le.text()

        if self.btn_VAR.isChecked():
            if len(edition) > 0:
                valid_edition = self.stack.add(edition)
                if valid_edition:
                    self.stack.user_vars[user_var] = self.stack.items[0]
                    lbl.setText(self.short_label_num(self.stack.user_vars[user_var]))
                    self.stack.remove()
                    self.le.clear()
                else:
                    self.le.clear()
                    self.statusbar.showMessage('Malformed expression', pc.TIMEOUT)
            else:
                if len(self.stack) > 0:
                    self.stack.user_vars[user_var] = self.stack.items[0]
                    lbl.setText(self.short_label_num(self.stack.user_vars[user_var]))
                    self.stack.remove()

            self.btn_VAR.setChecked(False)
            self.refresh_screen()

        else:
            if len(lbl.text()) > 0:  # only act if something is stored, ignore otherwise
                if len(edition) > 0:
                    valid_edition = self.stack.add(edition)
                    if valid_edition:
                        self.le.setText(str(self.stack.user_vars[user_var]))
                    else:
                        self.le.clear()
                        self.statusbar.showMessage('Malformed expression', pc.TIMEOUT)
                else:
                    self.le.setText(str(self.stack.user_vars[user_var]))
        self.refresh_screen()


    # Saves all relevant data for persistance purposes
    def save_data(self):
        """
        Saves all relevant data in the session to a json file in order to
        be able to load it in the next session.
        """
        data = {}
        data['stack'] = list(self.stack.items)  # to list in order to be serializable
        data['edition'] = self.le.text()
        data['variables'] = self.stack.user_vars

        s = json.dumps(data)
        with open(pc.DATA_JSON, 'w') as file:
            file.write(s)


    def load_data(self):
        """
        Loads data to the app from a json file written at the end of the previous session
        """
        if os.path.exists(pc.DATA_JSON):
            with open(pc.DATA_JSON, 'r') as file:
                s = file.read()
            data = json.loads(s)

            self.stack.items = deque(data['stack'])
            self.stack.user_vars = data['variables']
            self.le.setText(data['edition'])
            self.lbl_a.setText(self.short_label_num(data['variables']['A']))
            self.lbl_b.setText(self.short_label_num(data['variables']['B']))
            self.lbl_c.setText(self.short_label_num(data['variables']['C']))
            self.lbl_d.setText(self.short_label_num(data['variables']['D']))
            self.refresh_screen()


    def closeEvent(self, e):
        """
        Reimplementing closeEvent in order to save the data of the session
        automatically before leaving
        """
        self.save_data()
        self.close()
import math
import PyQt5
from PyQt5.QtWidgets import QLabel, QPushButton
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QColor
from collections import deque

import project_constants as pc



class Stack_Calculator():
    '''
    Represents a Stack with:
    - LIFO behaviour (add, remove)
    - Typical Stack Calculator Behaviour: only first 2 elements are
      in place to be operated with the common calc operators (sum, minus, etc...)

    Implementation based on python deque.
    '''

    def __init__(self, items=deque(), user_vars={'A': 0, 'B': 0, 'C': 0, 'D': 0,}):
        self.items = items
        self.user_vars = user_vars


    def __len__(self):
        return len(self.items)


    def __str__(self):
        return str(list(self.items))


    def add(self, element):
        '''
        Adds an element to the top only if is integer or float
        '''
        try:
            if isinstance(element, float):
                self.items.appendleft(element)
            else:
                self.items.appendleft(int(element))
            return True
        except ValueError:
            try:
                self.items.appendleft(float(element))
                return True
            except ValueError:
                return False


    def remove(self):
        '''
        Removes the element from the top
        '''
        if len(self.items) > 0:
            self.items.popleft()


    def to_first(self, position):
        '''
        Puts the position-element to the top
        '''
        if position < len(self.items):
            self.add(self.items[position])
            del self.items[position+1]


    def swap(self):
        '''
        Puts the second element to the top
        Particular case of to_first with position = 1
        '''
        self.to_first(1)


    def operate(self, op):
        '''
        Performs the specified operation.
        - If the operator is binary the operation is made with the 2 first
            elements of the stack
        - If the operator is unary the operation is made with the first element
            of the stack

        Available operations:
        - Binary: '+', '-', '*', '/', 'x^y'
        - Unary: '+/-', '1/x', 'sqrt', 'log', 'ln'
        '''

        if op in ['+', '-', '*', '/', 'x^y']:

            if len(self.items) > 1:
                if op == '+':
                    result = self.items[1] + self.items[0]
                elif op == '-':
                    result = self.items[1] - self.items[0]
                elif op == '*':
                    result = self.items[1] * self.items[0]
                elif op == '/':
                    if self.items[0] == 0:
                        return 'Division by Zero not allowed'
                    result = self.items[1] / self.items[0]
                elif op == 'x^y':
                    if self.items[1] == 0 and self.items[0] < 0:
                        return 'Raising 0 to a negative power not allowed'
                    result = self.items[1] ** self.items[0]

                self.items[1] = result
                self.remove()

        if op in ['+/-', '1/x', 'sqrt', 'log', 'ln']:

            if len(self.items) > 0:
                if op == '+/-':
                    result = self.items[0] * (-1)
                elif op == '1/x':
                    if self.items[0] != 0:
                        result = 1 / self.items[0]
                    else:
                        result = self.items[0]   # do nothing
                        return 'Division by Zero not allowed'
                elif op == 'sqrt':
                    if self.items[0] > 0:
                        result = math.sqrt(self.items[0])
                    else:
                        result = self.items[0]   # do nothing
                        return 'Square Root of a negative number not allowed'
                elif op == 'log':
                    if self.items[0] > 0:
                        result = math.log10(self.items[0])
                    else:
                        result = self.items[0]   # do nothing
                        return 'log10 of a negative number not allowed'
                elif op == 'ln':
                    if self.items[0] > 0:
                        result = math.log(self.items[0])
                    else:
                        result = self.items[0]   # do nothing
                        return 'ln of a negative number not allowed'

                self.items[0] = result


class Stack_Label(QLabel):
    """
    QLabel with some custom style
    """

    def __init__(self, *args):
        super().__init__(*args)

        font = QFont()
        font.setFamily(pc.FONT_FAMILY)
        font.setPointSize(pc.FONT_SIZE_BIG)
        font.setBold(True)

        self.setFont(font)
        self.setAlignment(Qt.AlignRight)


class Stack_QPushButton(QPushButton):
    """
    QPushButton with some custom style
    """

    def __init__(self, type, *args):
        super().__init__(*args)

        if type == 'special':
            bg_color = pc.BG_BUTTONS
            text_color = QColor(pc.TEXT_SPECIAL)
        elif type == 'disabled':
            bg_color = pc.BG_BUTTONS
            text_color = QColor(pc.TEXT_DISABLED)
        else:
            bg_color = pc.BG_BUTTONS
            text_color = QColor(pc.TEXT_REGULAR)

        font = QFont()
        font.setFamily(pc.FONT_FAMILY)
        font.setPointSize(pc.FONT_SIZE_MEDIUM)
        font.setBold(True)

        self.setFont(font)
        self.setStyleSheet(f"\
                Stack_QPushButton {{   \
                    background-color: {bg_color.name()}; \
                    color: {text_color.name()};  \
                }}   \
                Stack_QPushButton:checked {{\
                    background-color: {pc.TEXT_SPECIAL};\
                }}\
                ")

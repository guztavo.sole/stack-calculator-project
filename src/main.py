import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import QTranslator, QFile, QIODevice, QTextStream
import multiprocessing

import project_constants as pc
from interface import MainWindow



if __name__ == '__main__':
    

    app = QApplication(sys.argv)
    
    # # Translator
    # translator = QTranslator(app)
    # translator.load('qtbase_es.qm', pc.GUI_TRANSLATIONS_PATH)
    # app.installTranslator(translator)

    # # main style css
    # stream = QFile(pc.CSS_PATH)
    # stream.open(QIODevice.ReadOnly)
    # app.instance().setStyleSheet(QTextStream(stream).readAll())

    app.setWindowIcon(QIcon(pc.APP_ICON))

    # # os specific style
    # if 'windowsvista' in QStyleFactory.keys():
    #     app.setStyle(QStyleFactory.create('windowsvista'))
    # elif "Fusion" in QStyleFactory.keys():
    #     app.setStyle(QStyleFactory.create('Fusion'))

    # # multiprocessing
    # multiprocessing.freeze_support()

    gui = MainWindow()
    sys.exit(app.exec_())


import sys
from pathlib import Path
from PyQt5.QtGui import QFont, QColor



MODE = 'PRO'
APP_NAME = 'Stack Calculator'
VERSION = 2.0
SIGNATURE = 'gsd/2022'


# Paths
PROJECT_DIR = str(Path(__file__).resolve().parent.parent)
DATA_DIR = str(Path(PROJECT_DIR) / 'data')
DEPENDENCIES_DIR = str(Path(PROJECT_DIR) / '.dependencies')
IMAGES_DIR = str(Path(sys._MEIPASS) / '.images') if getattr(sys, 'frozen', False) else \
    str(Path(DEPENDENCIES_DIR) / 'images')

# Files
APP_ICON = str(Path(IMAGES_DIR) / 'calculator-icon.png')
DATA_JSON = str(Path(sys.executable).resolve().parent / '.data.json') if getattr(sys, 'frozen', False) else \
    str(Path(DATA_DIR) / '.data.json')


# Size and position
OFFSET = 100  # distance from top-left screen corner when launching the app
GLOBAL_WIDTH = 400
GLOBAL_HEIGHT = 700

# Color Interface
APP_BG_COLOR = 'slategrey'
SCREEN_BG_COLOR = 'ivory'
LINE_EDIT_TEXT = 'slategrey'
# Color Buttons
BG_BUTTONS = QColor(50,50,50)
TEXT_REGULAR = 'ivory'
TEXT_SPECIAL = 'turquoise'
TEXT_DISABLED = 'dimgray'


# Fonts
FONT_FAMILY = 'Ubuntu Mono'
FONT_SIZE_BIG = 24
FONT_SIZE_MEDIUM = 16


# Timeout of Messages in statusbar
TIMEOUT = 3000 #3s